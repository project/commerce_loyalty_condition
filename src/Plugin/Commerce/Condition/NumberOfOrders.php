<?php

namespace Drupal\commerce_loyalty_condition\Plugin\Commerce\Condition;

use Drupal\commerce\Plugin\Commerce\Condition\ConditionBase;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the order quantity for current user condition.
 *
 * @CommerceCondition(
 *   id = "number_of_orders",
 *   label = @Translation("Limit by number of orders"),
 *   display_label= @Translation("Limit by number of orders"),
 *   category = @Translation("Customer"),
 *   entity_type = "commerce_order",
 * )
 */
class NumberOfOrders extends ConditionBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'number_of_orders' => '',
        'orders_sum_minimum' => '',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['number_of_orders'] = [
      '#type' => 'number',
      '#title' => $this->t("Customer's quantity of completed orders"),
      '#description' => $this->t('Enter the quantity of orders the customer must have placed so this condition applies'),
      '#default_value' => $this->configuration['number_of_orders'],
      '#required' => TRUE,
      '#min' => 1,
    ];

    $form['number_days_in_the_past'] = [
      '#type' => 'number',
      '#title' => $this->t("How many days in the past should the oldest order be?"),
      '#description' => $this->t('Leave empty to ignore this condition.'),
      '#default_value' => $this->configuration['number_days_in_the_past'],
      '#required' => FALSE,
    ];

    $form['orders_sum_minimum'] = [
      '#type' => 'commerce_price',
      '#title' => $this->t("Customer's minimum total purchases sum"),
      '#description' => $this->t('Enter the minimum total amount of all orders placed by the customer. Leave empty to ignore this condition.'),
      '#default_value' => $this->configuration['orders_sum_minimum'],
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['number_of_orders'] = $values['number_of_orders'];
    $this->configuration['number_days_in_the_past'] = $values['number_days_in_the_past'];
    $this->configuration['orders_sum_minimum'] = $values['orders_sum_minimum'];
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    $this->assertEntity($entity);
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $entity;
    $customer = $order->getCustomer();
    $number_of_orders = $this->configuration['number_of_orders'];
    $number_days_in_the_past = isset($this->configuration['number_days_in_the_past']) ? \Drupal::time()
        ->getRequestTime() - $this->configuration['number_days_in_the_past'] * 86400 : NULL;
    $orders_sum_minimum = (float) $this->configuration["orders_sum_minimum"]["number"];

    $query = \Drupal::entityQuery('commerce_order')
      ->condition('uid', $customer->id(), '=')
      ->condition('completed', NULL, 'IS NOT NULL');

    if ($number_days_in_the_past) {
      $query->condition('completed', $number_days_in_the_past, '>');
    }
    else {
      $query->condition('completed', NULL, 'IS NOT NULL');
    }
    $order_ids = $query->execute();

    if (count($order_ids) < $number_of_orders) {
      return FALSE;
    }

    if ($orders_sum_minimum) {
      $orders = Order::loadMultiple($order_ids);
      foreach ($orders as $order) {
        $temp[] = $order->getTotalPrice()->getNumber();
      }
      $orders_total = array_sum($temp);
      return (bool) $orders_total > $orders_sum_minimum;
    }
    return TRUE;
  }

}
